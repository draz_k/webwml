#use wml::debian::template title="Debian GNU/NetBSD" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="c9a7e0f78250fe2fea728e669907c9ee47374e1c"


#############################################################################
<div class="important">
<p><strong>
Anpassningsarbetet har övergivits sedan länge. Det har inte fått några
uppdateringar sedan oktober 2002. Informationen på denna sida är endast
för historiska ändamål.
</strong></p>
</div>


<h1>
Debian GNU/NetBSD
</h1>


<p>
Debian GNU/NetBSD (i386) var en anpassning av operativsystemet Debian för 
NetBSD-kärnan och libc (inte att förväxlas med andra Debian BSD-anpassningar
baserade på glibc). Vid tillfället då den övergavs (runt oktober 2002), var den
i ett tidigt skede av utvecklingen, och kunde installeras från scratch.
</p>

<p>
Det fanns även ett försök att starta en Debian GNU/NetBSD (alpha)-anpassning,
som kunde köras från en chroot i ett inbyggt NetBSD (alpha)-system, men som
inte kunde starta själv och använde de flesta av de inbyggda NetBSD-biblioteken.
En <a
href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200201/msg00203.html">statusuppdatering</a>
skickades till sändlistan.
</p>

<h2>Historiska nyheter</h2>

<dl class="gloss">
  <dt class="new">2002-10-06:</dt>
  <dd>
      Experimentella installationsfloppies finns nu tillgängliga för att
      installera ett Debian GNU/NetBSD-system.
  </dd>
  <dt>2002-03-06:</dt>
  <dd>
      Matthew hackade ihop <a href="https://packages.debian.org/ifupdown">ifupdown</a>
      till ett fungerande tillstånd.
  </dd>
  <dt>2002-02-25:</dt>
  <dd>
      Matthew har rapporterat att shadow-stöd och PAM nu fungerar på NetBSD.
      <a href="https://packages.debian.org/fakeroot">fakeroot</a> verkar
      fungera på FreeBSD, men har fortfarande problem på NetBSD.
  </dd>
  <dt>2002-02-07:</dt>
  <dd>
      Nathan har precis <a
      href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00091.html">rapporterat</a>
      att han har fått Debian GNU/FreeBSD att start multiuser. Utöver detta har
      han jobbat på en endast-paket-installation (med hjälp av en hackad
      debootstrap) med en avsevärt mindre tarboll.
  </dd>
  <dt>2002-02-06:</dt>
  <dd>
      Enligt Joel klarade gcc-2.95.4 de flesta av testerna i testuppsättningen
      och är paketerad.
  </dd>
  <dt>2002-02-06:</dt>
  <dd>X11 fungerar på NetBSD!  Åter igen, kudos till Joel Baker
  </dd>
  <dt>2002-02-04:</dt>
  <dd>Första stegen till ett Debian/*BSD-arkiv: <br />
      <a href="mailto:lucifer@lightbearer.com">Joel Baker</a>
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00067.html">
      tillkännagav</a> ett <kbd>dupload</kbd>-bart arkiv för FreeBSD- och
      NetBSD-Debianpaket.
  </dd>
  <dt>2002-02-03:</dt>
  <dd>Debian GNU/NetBSD är nu 
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00043.html">
      värd för sig själv</a>!  Notera att den fortfarande behöver en fungerande
      NetBSD för installation.
  </dd>
  <dt>2002-01-30:</dt>
  <dd>Debian GNU/*BSD-anpassningen har nu en webbsida!</dd>
</dl>

<h2>Varför Debian GNU/NetBSD?</h2>



<ul>
<li>NetBSD kör på hårdvara som inte stöds av Linux. Att anpassa Debian till
NetBSD-kärnan ökar antalet plattformar som kan köra ett Debian-baserat
operativsystem.</li>

<li>Debian GNU/Hurd-projektet demonstrerar att Debian inte är bundet
till en specifik kärna. Dock så var Hurd-kärnan fortfarande relativt
omogen - Ett Debian GNU/NetBSD-system skulle vara användbart på
produktionsnivå.</li>

<li>Lektioner som har lärts från anpassningen av Debian till NetBSD kan
användas vid anpassning av Debian till andra kärnor (så som <a
href="https://www.freebsd.org/">FreeBSD</a> och <a
href="https://www.openbsd.org/">OpenBSD</a>).</li>

<li>I kontrast till projekt som <a href="https://www.finkproject.org/">Fink</a>,
existerar inte Debian NetBSD för att tillhandahålla extra mjukvara eller
en Unix-liknande miljö till ett existerande operativsystem (*BSD ports-träd
är redan omfattande och de tillhandahåller utan tvekan ett Unix-liknande
system). Istället skulle en användare eller administratör som är van vid
ett mer traditionellt Debiansystem känna sig bekväm med ett Debian
GNU/NetBSD-system direkt, och kompetent efter en relativt kort tid.</li>

<li>Inte alla gillar *BSF-ports-trädet eller *BSDs användarland (detta
är personlig preferens, och inte någon kommentar rörande kvalitet).
Linuxdistributioner har producerats som tillhandahåller *BSD-liknande
anpassningar eller en *BSD-liknande användarland för dem som gillar
BSDs användarmiljö men som även vill använda linuxkärnan - Debian GNU/NetBSD
är den logiska reversen av detta, och tillåter folk som gillar
GNU användarland eller ett paketsystem i Linux-stil med NetBSD-kärnan.
</li>

<li>Eftersom vi kan.</li>
</ul>


<h2>
Resurser
</h2>


<p>
Det finns en Debian GNU/*BSD-sändlista. Det mesta av de historiska
diskussionerna skedde där, och finns tillgängliga i webbarkiven på
<url "https://lists.debian.org/debian-bsd/" />.
</p>



## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
