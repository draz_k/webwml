#use wml::debian::template title="Var utvecklarna finns"
#use wml::debian::translation-check translation="6c996020eca890cac2a0243d4633d9bfa7935674"

<p>Många har visat sig intresserade av information om var
Debianutvecklarna egentligen finns.
Därför har vi valt att lägga in, som en del av utvecklardatabasen,
ett fält där utvecklare kan ange sina världskoordinater.

<p>Kartan nedan genererades från en
<a href="developers.coords">lista över utvecklarkoordinater</a>
(med namnen borttagna) med hjälp av programmet
<a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>.

<p><img src="developers.map.jpeg" alt="Världskarta">

<p>Om du är utvecklare och önskar lägga till dina koordinater till
din post i databasen, logga in på
<a href="https://db.debian.org">Debians utvecklardatabas</a>
och modifiera din listning.
Om du inte vet koordinaterna för din hemstad så bör du kunna hitta dem
på någon av platserna nedan:

<ul>
<li><a href="https://osm.org">Openstreetmap</a>
Du kan söka efter din stad i sökfältet.
Välj riktningspilarna bredvid sökfältet. Dra sedan den gröna markören
på OSM-kartan. Koordinaterna kommer sedan att dyka upp i 'från'-fältet.
</ul>

<p>Formatet på koordinaterna är ett av följande:
<dl>
<dt>Decimala grader
<dd>Formatet är +-GGG.GGGGGGGGGGGGGGG.
    Detta är formatet program som xearth använder, och formatet många
    positionswebbplatser använder.
    Vanligtvis är dock precisionen begränsad till fyra eller fem decimaler.

<dt>Grader och minuter (DGM)
<dd>Formatet är +-GGGMM.MMMMMMMMMMMMM.
    Det är inte en aritmetisk typ, utan en packad representation av
    två separata enheter, grader och minuter.
    Denna utdata är vanlig från några typer av handhållna GPS-enheter och
    från GPS-meddelanden på NMEA-format.

<dt>Grader, minuter och sekunder (DGMS)
<dd>Formatet är +-GGGMMSS.SSSSSSSSSSS.
    Precis som DGM är detta inte en aritmetisk typ, utan en packad
    representation av tre separata enheter: grader, minuter och
    sekunder.
    Denna utdata kommer vanligtvis från webbplatser som ger tre värden
    för varje position.
    Om positionen t.ex ges som 34:50:12.24523 nord skulle den i
    DGMS-formatet bli +0345012.24523.
</dl>

<p>För latitud är + nord, för longitud är + öst.
Det är viktigt att ge tillräckligt många inledande nollor för att skilja
ut formatet som används om din position är mindre än två grader från en
nollpunkt.
