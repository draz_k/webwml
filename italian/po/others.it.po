# Debian Web Site. others.it.po
# Copyright (C) 2002 Giuseppe Sacco.
# Giuseppe Sacco <eppesuigoccas@libero.it>, 2002.
# Giuseppe Sacco <eppesuig@debian.org>, 2004, 2005.
# Luca Monducci <luca.mo@tiscali.it>, 2007-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: others.it\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-05-06 15:27+0200\n"
"Last-Translator: Sebastiano Pistore <SebastianoPistore.info@protonmail.ch>\n"
"Language-Team: debian-l10n-italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Angolo dei Nuovi Membri"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Passo 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Passo 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Passo 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Passo 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Passo 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Passo 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Passo 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Checklist dei candidati"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Vedere <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</"
"a> (disponibile solo in francese) per maggiori informazioni."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Ulteriori informazioni"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Vedere <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/"
"</a> (disponibile solo in spagnolo) per maggiori informazioni."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Telefono"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Indirizzo"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Prodotti"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "Magliette"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "cappelli"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "adesivi"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "tazze"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "altro abbigliamento"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "polo"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "frisbee"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "tappetini per il mouse"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "tessere"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "cesti da pallacanestro"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "orecchini"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "valigie rigide"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "ombrelli"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "federe per cuscini"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "portachiavi"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "coltellini svizzeri"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "chiavette USB"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "cordini"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "altro"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "Lingue disponibili:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "Spedizione internazionale:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr "all'interno dell'Europa"

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr "Paese d'origine:"

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Fa donazioni a Debian"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr "I fondi sono usati per organizzare eventi legati al software libero"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Con&nbsp;la&nbsp;scritta&nbsp;&ldquo;Debian&rdquo;"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Senza&nbsp;la&nbsp;scritta&nbsp;&ldquo;Debian&rdquo;"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Encapsulated PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Potenziato da debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Potenziato da Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Potenziato da Debian]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (pulsante piccolo)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "come in precedenza"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Da quanto tempo usi Debian?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Sei una Sviluppatrice Debian?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "In quali aree di Debian sei convolta?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Cosa ti ha spinto a collaborare con Debian?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr "Hai suggerimenti per le donne interessate a collaborare con Debian?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr "Fai parte di altri gruppi di donne legati alle tecnologie? Quali?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Qualche informazione in più su di te..."

#~ msgid "Wanted:"
#~ msgstr "Richiesto:"

#~ msgid "Who:"
#~ msgstr "Chi:"

#~ msgid "Architecture:"
#~ msgstr "Architettura:"

#~ msgid "Specifications:"
#~ msgstr "Specifiche:"

#~ msgid "Where:"
#~ msgstr "Dove:"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Version"
#~ msgstr "Versione"

#~ msgid "Status"
#~ msgstr "Stato"

#~ msgid "Package"
#~ msgstr "Pacchetto"

#~ msgid "ALL"
#~ msgstr "TUTTI"

#~ msgid "Unknown"
#~ msgstr "Sconosciuto"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "BAD?"
#~ msgstr "Errato?"

#~ msgid "OK?"
#~ msgstr "OK?"

#~ msgid "BAD"
#~ msgstr "Errato"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Old banner ads"
#~ msgstr "Banner precedenti"

#~ msgid "Download"
#~ msgstr "Scarica"

#~ msgid "Unavailable"
#~ msgstr "Non disponibile"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "<void id=\"d-i\" />Sconosciuto"

#~ msgid "No images"
#~ msgstr "Nessuna immagine"

#~ msgid "No kernel"
#~ msgstr "Nessun kernel"

#~ msgid "Not yet"
#~ msgstr "Non ancora"

#~ msgid "Building"
#~ msgstr "Si compila"

#~ msgid "Booting"
#~ msgstr "Fa l'avvio"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (non funzionante)"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "Working"
#~ msgstr "Funzionante"
