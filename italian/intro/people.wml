#use wml::debian::template title="Le persone: chi siamo e cosa facciamo" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Giuseppe Sacco"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">Com'è cominciato tutto</a>
    <li><a href="#devcont">Sviluppatori e contributori</a>
    <li><a href="#supporters">Individui e organizzazioni che supportano Debian</a>
    <li><a href="#users">Utenti Debian</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Poiché molti l'hanno chiesto:
Debian si pronuncia <span style="white-space: nowrap;">/&#712;de.bi.&#601;n/.</span>
Prende il nome dal suo ideatore, Ian Murdock, e sua moglie, Debra.</p>
</aside>

<h2><a id="history">Com'è cominciato tutto</a></h2>

<p>Era l'agosto del 1993 quando Ian Murdock iniziò a lavorare su un nuovo
sistema operativo che sarabbe stato aperto, nello spirito di Linux e di GNU.
Mandò un invito aperto ad altri sviluppatori software, chiedendo loro di
contribuire ad una distribuzione di software basata sul kernel Linux, che
all'epoca era relativamente nuovo. Debian fu pensata per essere messa assieme
con cura e coscientemente, e mantenuta e supportata con egual cura,
abbracciando un design aperto e contributo e supporto dalla comunità
del software libero.</p>

<p>Iniziò come un piccolo e ristretto gruppo di hacker di software libero e
gradualmente crebbe fino a diventare una grande e organizzata comunità di
sviluppatori, contributori e utenti.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span><a href="$(DOC)/manuals/project-history/">Leggere la storia completa</a></button></p>

<h2><a id="devcont">Sviluppatori e contributori</a></h2>

<p>
Debian è una organizzazione di soli volontari. Oltre un migliaio di
sviluppatori attivi dai vari <a href="$(DEVEL)/developers.loc">angoli
del mondo</a> lavorano in Debian nel loro tempo libero. Alcuni tra noi
si sono incontrari di persona. Normalmente comunichiamo tramite email
(liste di messaggi su <a href="https://lists.debian.org/">lists.debian.org</a>)
e IRC (canale #debian at irc.debian.org).
</p>

<p>
L'elenco completo dei membri di Debian può essere trovato su
<a href="https://nm.debian.org/members">nm.debian.org</a>, mentre
<a href="https://contributors.debian.org">contributors.debian.org</a>
ha un elenco di tutti i contributori e gruppi che lavorano nella
distribuzione Debian.</p>

<p>Il Progetto Debian ha un'articolata <a href="organization">struttura
interna</a>. Per maggiori informazioni su come sia il progetto Debian
dall'interno, visitare l'<a href="$(DEVEL)/">angolo dello sviluppatore</a>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">Leggere della nostra Filosofia</a></button></p>

<h2><a id="supporters">Individui e organizzazioni che supportano Debian</a></h2>

<p>Oltre a sviluppatori e contributori, molti altri individui e organizzazioni
fanno parte della comunità Debian:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Sponsor di hosting e hardware</a></li>
  <li><a href="../mirror/sponsors">Sponsor dei mirror</a></li>
  <li><a href="../partners/">Development and service partners</a></li>
  <li><a href="../consultants">Consulenti</a></li>
  <li><a href="../CD/vendors">Rivenditori dei supporti per l'installazione</a></li>
  <li><a href="../distrib/pre-installed">Rivenditori di computer che offrono macchine con Debian pre-installato</a></li>
  <li><a href="../events/merchandise">Rivenditori di materiale promozionale</a></li>
</ul>

<h2><a id="users">Utenti Debian</a></h2>

<p>
Debian è usata da un vasto gruppo di organizzazioni, grandi e piccole, oltre
che da migliaia di individui. Vedere la nostra pagina <a
href="../users/">Chi usa Debian?</a> per un elenco di organizzazioni legate
all'ambito educativo, commerciali e non-profit, oltre ad agenzie governative
che hanno inviato una breve descrizione di come e perché usano Debian.
</p>
