#use wml::debian::template title="Erratas del instalador de Debian"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="50ddc8fab8f8142c1e8266a7c0c741f9bfe1b23a" maintainer="Laura Arjona Reina"

<h1>Erratas en «<humanversion />»</h1>

<p>
Esta es una lista de problemas conocidos en la versión «<humanversion />»
del instalador de Debian. Si usted no ve aquí listado su problema,
por favor envíenos un <a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">informe de instalación</a>
describiéndolo.
</p>

<dl class="gloss">
     <dt>Modo de rescate roto con el instalador gráfico</dt>
     <dd>Durante las pruebas de la imagen RC1 de «bullseye» se descubrió
     que el el modo de rescate parece roto (<a href="https://bugs.debian.org/987377">#987377</a>).
     Adicionalmente, la etiqueta «Rescate» en la cabecera necesita ajustarse
     al tema gráfico de «bullseye».
     <br />
     <b>Estado:</b> Corregido en la RC 2 de «bullseye».</dd>

     <dt>El firmware amdgpu es necesario para muchas tarjetas gráficas de AMD</dt>
     <dd>Parece que hay una necesidad creciente de instalación de firmware amdgpu
     (a través del paquete no libre <code>firmware-amd-graphics</code>)
     para evitar una pantalla negra al iniciar el sistema instalado. En la RC 1
     de «bullseye», incluso usando una imagen de instalación que incluya todos los
     paquetes de firmware, el instalador no detecta la necesidad de ese componente
     específico. Consulte el
     <a href="https://bugs.debian.org/989863">informe de fallo paraguas</a>
     para llevar el seguimiento de nuestros esfuerzos.
     <br />
     <b>Estado:</b> Corregido en la RC 3 de «bullseye».</dd>
     
     <dt>Firmware necesario para algunas tarjetas de sonido</dt>
     <dd>Parece que hay algunas tarjetas de sonido que requieren cargar 
     firmware para poder emitir sonido. En <q>bullseye</q>, el instalador no
     puede cargarlo en una etapa inicial, lo que significa que la síntesis de
     voz durante la instalación no está disponible con esas tarjetas. Una
     alternativa es conectar otra tarjeta de sonido que no necesite dicho
     firmware.
     Consulte el <a href="https://bugs.debian.org/992699">informe de fallo paraguas</a>
     para llevar seguimiento de nuestros esfuerzos para arreglar esto.</dd>

<dt>Las instalaciones con escritorio pueden fallar si se utiliza solo el CD 1</dt>
<dd>Debido a restricciones de espacio en el primer CD, no caben en el CD 1 todos los 
paquetes que se esperan para el escritorio GNOME. Para una instalación exitosa, 
use fuentes de paquete adicionales (p. ej. un segundo CD o una réplica en red)
o use un DVD.
<br /> <b>Estado:</b> No es probable que se puedan hacer más esfuerzos para encajar paquetes
	en el CD 1.</dd>

     <dt>LUKS2 es incompatible con el soporte a cifrado de disco de GRUB</dt>
     <dd>Se descubrió tarde que GRUB no tiene soporte para LUKS2.
     Esto significa que los usuarios que quieren usar
       <tt>GRUB_ENABLE_CRYPTODISK</tt> y evitar una partición
       <tt>/boot</tt> separada y sin cifrar no podrán hacerlo
       (<a href="https://bugs.debian.org/927165">#927165</a>).  En cualquier caso, 
       esta configuración no está soportada en el instalador, pero podría tener
       sentido al menos documentar esta limitación de manera más prominente
       y tener al menos la posibilidad de optar por LUKS1 durante el proceso de
       instalación.
     <br />
     <b>Estado:</b> Se han expresado algunas ideas en el informe de fallo; los mantenedores de cryptsetup
     han escrito <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">documentación específica</a>.</dd>

</dl>
