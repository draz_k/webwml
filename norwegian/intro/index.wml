#use wml::debian::template title="Introduksjon til Debian" MAINPAGE="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c" maintainer="Hans F. Nordhaug"

<a id=community></a>
<h2>Debian er et fellesskap av folk</h2>
<p>Tusensvis av frivillige i hele verden arbeider sammen med fri 
  programvare som mål og for brukernes behov.</p>

<ul>
  <li>
    <a href="people">Folk:</a>
    Hvem vi er, hva vi gjør
  </li>
  <li>
    <a href="philosophy">Filosofi:</a>
    Hvorfor vi gjør det, og hvordan vi gjør det
  </li>
  <li>
    <a href="../devel/join/">Bli involvert:</a>
    Du kan bli del av dette!
  </li>
  <li>
    <a href="help">Hvordan kan du hjelpe Debian?</a>
  </li>
  <li>
    <a href="../social_contract">Sosial kontrakt:</a>
    Vår moralske agenda
  </li>
  <li>
    <a href="diversity">Mangfoldserklæring</a>
  </li>
  <li>
    <a href="../code_of_conduct">Etiske retningslinjer</a>
  </li>
  <li>
    <a href="../partners/">Partnere:</a>
    Bedrifter og organisasjoner som løpende hjelper Debian-prosjektet
  </li>
  <li>
    <a href="../donations">Donasjoner</a>
  </li>
  <li>
    <a href="../legal/">Juridisk informasjon</a>
  </li>
  <li>
    <a href="../legal/privacy">Personvernregler</a>
  </li>
  <li>
    <a href="../contact">Kontakt oss</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Debian er et fritt operativsystem</h2>
<p>Vi starter med Linux og legger til mange tusen applikasjoner for å møte brukernes behov.</p>

<ul>
  <li>
    <a href="../distrib">Last ned:</a>
    Flere varianter av Debian-bildefiler
  </li>
  <li>
  <a href="why_debian">Hvorfor Debian</a>
  </li>
  <li>
    <a href="../support">Støtte:</a>
    Få hjelp
  </li>
  <li>
    <a href="../security">Sikkerhet:</a>
    Siste oppdatering <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages"> Programvarepakker:</a>
    Søk i og utforsk den lange listen med vår programvare
  </li>
  <li>
    <a href="../doc"> Dokumentasjon</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> Debian wiki</a>
  </li>
  <li>
    <a href="../Bugs"> Feilrapporter</a>
  </li>
  <li>
    <a href="https://lists.debian.org/"> E-postlister</a>
  </li>
  <li>
    <a href="../blends"> Pure Blends:</a>
    Metapakker for spesifikke behow
  </li>
  <li>
    <a href="../devel"> Utviklerhjørnet:</a>
    Informasjon som hovedsaklig er nyttig for Debian-utviklere
  </li>
  <li>
    <a href="../ports"> Arkitekturtilpasninger:</a>
    CPU-arkitekturer vi støtter
  </li>
  <li>
    <a href="search">Informasjon om hvordan bruke Debians søkemotor</a>.
  </li>
  <li>
    <a href="cn">Informasjon om sider som er tilgjengelig på flere språk</a>.
  </li>
</ul>
