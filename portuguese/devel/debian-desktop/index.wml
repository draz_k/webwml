#use wml::debian::template title="Debian no Desktop" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="385b72248b2434177dbd472f7c322a759a6a8dd0"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#philosophy">Nossa filosofia</a></li>
<li><a href="#help">Como você pode ajudar</a></li>
<li><a href="#join">Junte-se a nós</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>O Debian Desktop é um grupo de
voluntários(as) que desejam criar o melhor sistema operacional possível para
uso doméstico e em estações de trabalho corporativas. Nosso lema: <q>software
que simplesmente funciona.</q> Nosso objetivo: trazer o Debian, o GNU e o Linux
para o mundo mainstream.</p>
</aside>

<h2><a id="philosophy">Nossa filosofia</a></h2>

<p>
Reconhecemos que muitos
<a href="https://wiki.debian.org/DesktopEnvironment">ambientes desktop</a>
existem e que seu uso será suportado – isso inclui certificar que funcionam bem
no Debian. Nosso objetivo é tornar as interfaces gráficas fáceis de usar para
iniciantes, permitindo que usuários(as) avançados(as) e especialistas ajustem as
coisas se quiserem.
</p>

<p>
Tentaremos garantir que o software seja configurado para o uso mais comum no
desktop. Por exemplo, a conta de usuário(a) normal criada durante a instalação
deve ter permissão para reproduzir áudio e vídeo, realizar impressões e
gerenciar o sistema por meio do sudo. Além disso, gostaríamos de manter as
questões do <a href="https://wiki.debian.org/debconf">debconf</a> (o sistema de
gerenciamento de configuração do Debian) no mínimo absoluto.
Não há necessidade de apresentar detalhes técnicos difíceis durante a
instalação. Em vez disso, tentaremos garantir que as questões do debconf
façam sentido para os(as) usuários(as). Um(a) novato(a) pode nem mesmo entender
do que tratam essas perguntas. Um(a) especialista, no entanto, fica
perfeitamente feliz em configurar o ambiente dekstop após a conclusão da
instalação.
</p>

<h2><a id="help">Como você pode ajudar</a></h2>

<p>
Procuramos pessoas motivadas que fazem as coisas acontecerem. Você não precisa
ser um(a) desenvolvedor(a) Debian (DD) para enviar patches ou fazer pacotes.
A equipe central do Debian Desktop irá garantir que seu trabalho seja integrado.
Então, aqui estão algumas coisas que você pode fazer para ajudar:
</p>

<ul>
  <li>Teste o ambiente de área de trabalho padrão (ou qualquer um dos outros desktops) para
a próxima versão. Obtenha uma das
<a href="$(DEVEL)/debian-installer/">imagens teste (testing)</a> e envie
feedback (em inglês) para a
<a href="https://lists.debian.org/debian-desktop/">lista de discussão debian-desktop</a>.</li>
  <li>Junte-se à
<a href="https://wiki.debian.org/DebianInstaller/Team">equipe do instalador Debian</a>
e ajude a melhorar o <a href="$(DEVEL)/debian-installer/">debian-installer</a> – a
interface GTK+ precisa de você.</li>
  <li>Você pode ajudar a
<a href="https://wiki.debian.org/Teams/DebianGnome">equipe GNOME do Debian</a>,
os(as) <a href="https://qt-kde-team.pages.debian.net/">mantenedores(as) Debian Qt/KDE e a equipeDebian KDE Extras</a>,
ou o <a href="https://salsa.debian.org/xfce-team/">Grupo Debian Xfce</a> com
empacotamento, correção de bugs, documentação, testes e muito mais.</li>
  <li>Ajude a melhorar o
<a href="https://packages.debian.org/debconf">debconf</a> diminuindo a
prioridade das perguntas ou removendo as perguntas desnecessárias dos pacotes.
Torne as perguntas necessárias do debconf mais fáceis de entender.</li>
  <li>Tem habilidades de designer? Por que não trabalhar no
<a href="https://wiki.debian.org/DebianDesktop/Artwork">Debian Desktop Artwork</a>.</li>
</ul>

<h2><a id="join">Junte-se a nós</a></h2>

<aside class="light">
  <span class="fa fa-users fa-4x"></span>
</aside>

<ul>
   <li><strong>Wiki:</strong> visite nossa wiki
<a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a> (alguns
artigos podem estar desatualizados).</li>
   <li><strong>Lista de discussão:</strong> converse conosco (em inglês) na
lista de discussão
<a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.</li>
   <li><strong>Canal IRC: </strong>converse conosco, em inglês, no IRC.
Junte-se ao canal #debian-desktop no
<a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org)</li>
</ul>

