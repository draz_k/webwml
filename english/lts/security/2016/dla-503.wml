<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in libxml2, a library providing
support to read, modify and write XML and HTML files. A remote attacker
could provide a specially crafted XML or HTML file that, when processed
by an application using libxml2, would cause a denial-of-service against
the application, or potentially the execution of arbitrary code with the
privileges of the user running the application.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.8.0+dfsg1-7+wheezy6.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-503.data"
# $Id: $
