<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor. The
Common Vulnerabilities and Exposures project identifies the following
problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9379">CVE-2016-9379</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-9380">CVE-2016-9380</a> (XSA-198)

    <p>pygrub, the boot loader emulator, fails to quote (or sanity check)
    its results when reporting them to its caller.  A malicious guest
    administrator can obtain the contents of sensitive host files</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9381">CVE-2016-9381</a> (XSA-197)

    <p>The compiler can emit optimizations in qemu which can lead to double
    fetch vulnerabilities.  Malicious administrators can exploit this
    vulnerability to take over the qemu process, elevating its privilege
    to that of the qemu process. </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9382">CVE-2016-9382</a> (XSA-192)

    <p>LDTR, just like TR, is purely a protected mode facility.  Hence even
    when switching to a VM86 mode task, LDTR loading needs to follow
    protected mode semantics.  A malicious unprivileged guest process
    can crash or escalate its privilege to that of the guest operating
    system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9383">CVE-2016-9383</a> (XSA-195)

    <p>When Xen needs to emulate some instruction, to efficiently handle
    the emulation, the memory address and register operand are
    recalculated internally to Xen.    In this process, the high bits of
    an intermediate expression were discarded, leading to both the
    memory location and the register operand being wrong.  A malicious
    guest can modify arbitrary memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9386">CVE-2016-9386</a> (XSA-191)

    <p>The Xen x86 emulator erroneously failed to consider the unusability
    of segments when performing memory accesses.  An unprivileged guest
    user program may be able to elevate its privilege to that of the
    guest operating system.</p>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.6.lts1-4.  For Debian 8 <q>Jessie</q>, these problems will be fixed shortly.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-720.data"
# $Id: $
