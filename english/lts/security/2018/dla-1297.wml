<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Leon reported five heap-based buffer-overflow vulnerabilities in FreeXL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7435">CVE-2018-7435</a>

    <p>There is a heap-based buffer over-read in the freexl::destroy_cell
    function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7436">CVE-2018-7436</a>

    <p>There is a heap-based buffer over-read in a pointer dereference of
    the parse_SST function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7437">CVE-2018-7437</a>

    <p>There is a heap-based buffer over-read in a memcpy call of the
    parse_SST function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7438">CVE-2018-7438</a>

    <p>There is a heap-based buffer over-read in the parse_unicode_string
    function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7439">CVE-2018-7439</a>

    <p>There is a heap-based buffer over-read in the function
    read_mini_biff_next_record.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0.0b-1+deb7u5.</p>

<p>We recommend that you upgrade your freexl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1297.data"
# $Id: $
