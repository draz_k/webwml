<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>OpenOCD, an on-chip JTAG debug solution for ARM and MIPS systems, does
not block attempts to use HTTP POST for sending data to localhost, which
allows remote attackers to conduct cross-protocol scripting attacks,
and consequently execute arbitrary commands, via a crafted web site.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.5.0-1+deb7u1.</p>

<p>We recommend that you upgrade your openocd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1253.data"
# $Id: $
