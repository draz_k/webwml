<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The http2 server support in this package was vulnerable to
certain types of DOS attacks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9512">CVE-2019-9512</a>

    <p>This code was vulnerable to ping floods, potentially leading to a denial of
    service. The attacker sends continual pings to an HTTP/2 peer, causing the peer
    to build an internal queue of responses. Depending on how efficiently this data
    is queued, this can consume excess CPU, memory, or both.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9514">CVE-2019-9514</a>

    <p>This code was vulnerable to a reset flood, potentially leading to a denial
    of service. The attacker opens a number of streams and sends an invalid request
    over each stream that should solicit a stream of RST_STREAM frames from the
    peer. Depending on how the peer queues the RST_STREAM frames, this can consume
    excess memory, CPU, or both.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:0.0+git20161013.8b4af36+dfsg-3+deb9u1.</p>

<p>We recommend that you upgrade your golang-golang-x-net-dev packages.</p>

<p>For the detailed security status of golang-golang-x-net-dev please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-golang-x-net-dev">https://security-tracker.debian.org/tracker/golang-golang-x-net-dev</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2485.data"
# $Id: $
