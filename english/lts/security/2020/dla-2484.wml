<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Let's Encrypt's ACMEv1 API is deprecated and in the process of being
shut down. Beginning with brownouts in January 2021, and ending with
a total shutdown in June 2021, the Let's Encrypt APIs will become
unavailable. To prevent users having disruptions to their certificate
renewals, this update backports the switch over to the ACMEv2 API.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.28.0-1~deb9u3.</p>

<p>We recommend that you upgrade your python-certbot packages.</p>

<p>For the detailed security status of python-certbot please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-certbot">https://security-tracker.debian.org/tracker/python-certbot</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2484.data"
# $Id: $
