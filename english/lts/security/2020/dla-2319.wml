<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>xrdp-sesman service in xrdp can be crashed by connecting over port 3350 and
supplying a malicious payload. Once the xrdp-sesman process is dead, an
unprivileged attacker on the server could then proceed to start their own
imposter sesman service listening on port 3350. This will allow them to capture
any user credentials that are submitted to XRDP and approve or reject arbitrary
login credentials. For xorgxrdp sessions in particular, this allows an
unauthorized user to hijack an existing session. This is a buffer overflow
attack, so there may be a risk of arbitrary code execution as well.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.9.1-9+deb9u4.</p>

<p>We recommend that you upgrade your xrdp packages.</p>

<p>For the detailed security status of xrdp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xrdp">https://security-tracker.debian.org/tracker/xrdp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2319.data"
# $Id: $
