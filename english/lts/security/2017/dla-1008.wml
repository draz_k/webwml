<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7375">CVE-2017-7375</a>

      <p>Missing validation for external entities in xmlParsePEReference</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9047">CVE-2017-9047</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-9048">CVE-2017-9048</a>

      <p>A buffer overflow was discovered in libxml2 20904-GITv2.9.4-16-g0741801.
      The function xmlSnprintfElementContent in valid.c is supposed to
      recursively dump the element content definition into a char buffer <q>buf</q>
      of size <q>size</q>. The variable len is assigned strlen(buf).
      If the content->type is XML_ELEMENT_CONTENT_ELEMENT, then (i) the
      content->prefix is appended to buf (if it actually fits) whereupon
      (ii) content->name is written to the buffer. However, the check for
      whether the content->name actually fits also uses <q>len</q> rather than
      the updated buffer length strlen(buf). This allows us to write about
      <q>size</q> many bytes beyond the allocated memory. This vulnerability
      causes programs that use libxml2, such as PHP, to crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9049">CVE-2017-9049</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-9050">CVE-2017-9050</a>

      <p>libxml2 20904-GITv2.9.4-16-g0741801 is vulnerable to a heap-based
      buffer over-read in the xmlDictComputeFastKey function in dict.c.
      This vulnerability causes programs that use libxml2, such as PHP,
      to crash. This vulnerability exists because of an incomplete fix
      for libxml2 Bug 759398.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.8.0+dfsg1-7+wheezy8.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1008.data"
# $Id: $
