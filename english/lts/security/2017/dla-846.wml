<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that libzip-ruby, a Ruby module for reading and
writing zip files, is prone to a directory traversal vulnerability. An
attacker can take advantage of this flaw to overwrite arbitrary files
during archive extraction via a .. (dot dot) in an extracted filename.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.9.4-1+deb7u1.</p>

<p>We recommend that you upgrade your libzip-ruby packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-846.data"
# $Id: $
