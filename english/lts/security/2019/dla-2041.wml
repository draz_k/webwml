<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that debian-edu-config, the package containing the
configuration files and scripts for Debian Edu (Skolelinux), contained an
insecure configuration for kadmin, the Kerberos administration server.  The
insecure configuration allowed every user to change other users' passwords,
thus impersonating them and possibly gaining their privileges.</p>

<p>The bug was not exposed in the officially documented user management
frontends of Debian Edu, but could be abused by local network users knowing
how to use the Kerberos backend.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.818+deb8u3.</p>

<p>We recommend that you upgrade your debian-edu-config packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2041.data"
# $Id: $
