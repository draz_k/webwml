<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Simon Scannell of Ripstech Technologies discovered multiple
vulnerabilities in wordpress, a web blogging manager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8942">CVE-2019-8942</a>

    <p>remote code execution in wordpress because an _wp_attached_file Post
    Meta entry can be changed to an arbitrary string, such as one ending
    with a .jpg?file.php substring. An attacker with author privileges
    can execute arbitrary code by uploading a crafted image containing
    PHP code in the Exif metadata.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9787">CVE-2019-9787</a>

    <p>wordpress does not properly filter comment content, leading to
    Remote Code Execution by unauthenticated users in a default
    configuration. This occurs because CSRF protection is mishandled,
    and because Search Engine Optimization of A elements is performed
    incorrectly, leading to XSS. The XSS results in administrative
    access.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.1.26+dfsg-1+deb8u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1742.data"
# $Id: $
