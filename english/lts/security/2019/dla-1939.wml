<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues in poppler, a PDF rendering library, have been fixed.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20650">CVE-2018-20650</a>

     <p>A missing check for the dict data type could lead to a denial of
     service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21009">CVE-2018-21009</a>

     <p>An integer overflow might happen in Parser::makeStream.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12493">CVE-2019-12493</a>

     <p>A stack-based buffer over-read by a crafted PDF file might happen in
     PostScriptFunction::transform because some functions  mishandle tint
     transformation.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.26.5-2+deb8u11.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1939.data"
# $Id: $
