<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the previous upload of the package prosody
versioned 0.9.12-2+deb9u3 introduced a regression in the
mod_auth_internal_hashed module. Big thanks to Andre Bianchi for the reporting
an issue and for testing the update.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.9.12-2+deb9u4.</p>

<p>We recommend that you upgrade your prosody packages.</p>

<p>For the detailed security status of prosody please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/prosody">https://security-tracker.debian.org/tracker/prosody</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2687-2.data"
# $Id: $
