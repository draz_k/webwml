<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an integer-overflow vulnerability in
hiredis, a C client library for communicating with Redis databases.  This
occurred within the handling and parsing of 'multi-bulk' replies.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32765">CVE-2021-32765</a>

    <p>Hiredis is a minimalistic C client library for the Redis database. In
    affected versions Hiredis is vulnurable to integer overflow if provided
    maliciously crafted or corrupted `RESP` `mult-bulk` protocol data. When
    parsing `multi-bulk` (array-like) replies, hiredis fails to check if `count
    * sizeof(redisReply*)` can be represented in `SIZE_MAX`. If it can not, and
    the `calloc()` call doesn't itself make this check, it would result in a
    short allocation and subsequent buffer overflow. Users of hiredis who are
    unable to update may set the
    [maxelements](https://github.com/redis/hiredis#reader-max-array-elements)
    context option to a value small enough that no overflow is
    possible.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
0.13.3-1+deb9u1.</p>

<p>We recommend that you upgrade your hiredis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2783.data"
# $Id: $
