<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a cross-site scripting (XSS) vulnerability
in <tt>velocity-tools</tt>, a collection of useful tools for the "Velocity"
template engine.</p>

<p>The default error page could be exploited to steal session cookies,
perform requests in the name of the victim, used for phishing attacks
and many other similar attacks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13959">CVE-2020-13959</a>

    <p>The default error page for VelocityView in Apache Velocity Tools prior
    to 3.1 reflects back the vm file that was entered as part of the URL. An
    attacker can set an XSS payload file as this vm file in the URL which
    results in this payload being executed. XSS vulnerabilities allow attackers
    to execute arbitrary JavaScript in the context of the attacked website and
    the attacked user. This can be abused to steal session cookies, perform
    requests in the name of the victim or for phishing attacks.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
2.0-6+deb9u1.</p>

<p>We recommend that you upgrade your velocity-tools packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2597.data"
# $Id: $
