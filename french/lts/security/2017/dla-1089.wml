#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Quelques problèmes ont été trouvés dans Irssi :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10965">CVE-2017-10965</a>

<p>Un problème a été découvert dans Irssi avant 1.0.4. Lors de la réception de
messages avec des estampilles temporelles non valables, Irssi essayait de
déréférencer un pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10966">CVE-2017-10966</a>

<p>Un problème a été découvert dans Irssi avant 1.0.4. Lors de la mise à jour de
la liste interne des pseudos, Irssi pouvait incorrectement utiliser l’interface
GHashTable et libérer le pseudo lors de sa mise à jour. Cela pouvait aboutir
dans des conditions d’utilisation de mémoire après libération sur chaque accès
à la table de hachage.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.8.15-5+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets irssi.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1089.data"
# $Id: $
