#use wml::debian::translation-check translation="8868516293b623e10a6ec89aadb7da9305f73aa6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans librsvg, une bibliothèque
de rendu SVG. Cette mise à jour corrige quelques problèmes de déni de service
à cause du traitement exponentiel d’éléments, d’un épuisement de pile ou d’un
plantage d'application lors du traitement de fichiers contrefaits pour
l'occasion, ainsi que quelques problèmes de sécurité de la mémoire.
</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.40.21-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets librsvg.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de librsvg, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/librsvg">https://security-tracker.debian.org/tracker/librsvg</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2285.data"
# $Id: $
