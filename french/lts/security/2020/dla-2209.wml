#use wml::debian::translation-check translation="bb3f804000185056dc659c88709d05facf94aa3f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans la servlet
Tomcat et le moteur JSP.</p>

<p>ATTENTION : le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">CVE-2020-1938</a>
peut perturber les services reposant sur une configuration de travail AJP.
L’option secretRequired par défaut est réglée à « true » désormais. Vous devez
définir un secret dans votre server.xml ou revenir en arrière en réglant
secretRequired à « false ».</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17563">CVE-2019-17563</a>

<p>Lors de l’utilisation de l’authentification FORM avec Tomcat, il existait une
fenêtre étroite pendant laquelle un attaquant pouvait réaliser une attaque de
fixation de session. La fenêtre était considérée comme trop étroite pour
la réalisation d’un exploit, mais selon le principe de précaution, ce problème
a été traité comme une vulnérabilité de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1935">CVE-2020-1935</a>

<p>Dans Apache Tomcat le code d’analyse d’en-tête HTTP utilisait une méthode
d’analyse de fin de ligne qui permettait à quelques en-têtes HTTP non valables
d’être analysés comme valables. Cela conduisait à une possibilité de
dissimulation de requête HTTP si Tomcat résidait derrière un mandataire inverse
qui gérait incorrectement l’en-tête Transfer-Encoding non valable d’une certaine
manière. Un tel mandataire inverse est considéré comme peu probable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">CVE-2020-1938</a>

<p>Lors de l’utilisation du protocole JServ (AJP) d’Apache, une attention doit
être portée lors de l’acceptation de connexions entrantes vers Apache Tomcat.
Tomcat traite les connexions AJP comme plus fiables que, par exemple, une
connexion HTTP similaire. Si de telles connexions sont à la disposition d’un
attaquant, elles peuvent être exploitées de manière surprenante. Précédemment,
Tomcat fournissait un connecteur AJP activé par défaut qui écoutait sur toutes
les adresses IP configurées. Il était attendu (et recommandé dans le guide de
sécurité) que ce connecteur soit désactivé s’il n’était pas nécessaire.

Il est à remarquer que Debian désactivait déjà par défaut le connecteur AJP.
La mitigation est nécessaire seulement si le port AJP était rendu accessible à des
utilisateurs non authentifiés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a>

<p>Lors de l’utilisation d’Apache Tomcat et si a) un attaquant était capable de
contrôler le contenu et le nom d’un fichier sur le serveur, b) le serveur était
configuré pour utiliser PersistenceManager avec un FileStore, c) le
PersistenceManager était configuré avec sessionAttributeValueClassNameFilter="null"
(la valeur par défaut à moins qu’un SecurityManager soit utilisé ) ou qu’un
filtre assez faible permettait la désérialisation de l’objet fourni par
l’attaquant, d) l’attaquant connaissait le chemin relatif dans l'emplacement de
stockage utilisé par FileStore du fichier dont il avait le contrôle, alors, en
utilisant une requête spécialement contrefaite, l’attaquant était capable de
déclencher une exécution de code à distance par la désérialisation d’un fichier
sous son contrôle. Il est à remarquer que toutes les conditions (a à d) devaient
être satisfaites pour la réussite de l’attaque</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 8.0.14-1+deb8u17.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat8.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2209.data"
# $Id: $
