#use wml::debian::translation-check translation="38f80062e2abeb127c295e7a37df1331a66b5bf6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une possibilité d’attaque par dépassement
de tampon dans le serveur DNS bind9 causée par un problème dans la politique de
négociation de sécurité dans GSSAPI (« Generic Security Services »).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8625">CVE-2020-8625</a>
<p>Les serveurs BIND étaient vulnérables s’ils exécutaient une version affectée
et étaient configurés pour utiliser les fonctionnalités de GSS-TSIG. Dans une
configuration utilisant les réglages par défaut de BIND, le code de chemin
vulnérable n’est pas exposé, mais le serveur peut être rendu vulnérable en
réglant explicitement des valeurs autorisées pour les options de configuration
de tkey-gssapi-keytab ou tkey-gssapi-credentialconfiguration. Quoique la
configuration par défaut ne soit pas vulnérable, GSS-TSIG est couramment
utilisé dans les réseaux où BIND est intégré avec Samba, ainsi que dans les
environnements de serveurs mixtes qui combinent des serveurs BIND avec des
contrôleurs de domaine Active Directory. Le résultat le plus probable d’une
exploitation réussie de la vulnérabilité est un plantage du processus nommé.
Cependant, une exécution de code à distance, bien que non prouvée, est
théoriquement possible. Sont affectés les versions
BIND 9.5.0 -&gt; 9.11.27, 9.12.0 -&gt; 9.16.11 et les versions
BIND 9.11.3-S1 -&gt; 9.11.27-S1 et 9.16.8-S1 -&gt; 9.16.11-S1 de <q>BIND
Supported Preview Edition</q>. Sont aussi affectées les
versions 9.17.0 -&gt; 9.17.1 de la branche de développement BIND 9.17.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:9.10.3.dfsg.P4-12.3+deb9u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2568.data"
# $Id: $
