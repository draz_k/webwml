#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Le correctif original pour le <a href="https://security-tracker.debian.org/tracker/CVE-2015-7552">CVE-2015-7552</a> (DLA-450-1)
était incomplet.</p>

<p>Un dépassement de tas dans gdk-pixbuf, une bibliothèque offrant des
fonctions de chargement et d'enregistrement d'images, de mise à l'échelle
rapide et de combinaisons d'images en mémoire (« pixbufs »), permet à des
attaquants distants de provoquer un déni de service ou éventuellement
d'exécuter du code arbitraire à l'aide d'un fichier BMP contrefait.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.26.1-1+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gdk-pixbuf.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-501.data"
# $Id: $
