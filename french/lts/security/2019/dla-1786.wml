#use wml::debian::translation-check translation="42132d0143f457bd4e419337deee13bb90bc9e59" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été corrigés dans Qt4.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15518">CVE-2018-15518</a>

<p>Une double libération ou corruption de zone de mémoire lors de l’analyse d’un
document XML illégal contrefait pour l’occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19869">CVE-2018-19869</a>

<p>Une image SVG malformée pourrait causer une erreur de segmentation dans
qsvghandler.cpp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19870">CVE-2018-19870</a>

<p>Une image GIF malformée pourrait causer un déréférencement de pointeur NULL
dans QGifHandler, aboutissant à une erreur de segmentation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19871">CVE-2018-19871</a>

<p>Il existait une consommation de ressources incontrôlée dans QTgaFile.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19873">CVE-2018-19873</a>

<p>QBmpHandler possédait un dépassement de tampon à l’aide de données BMP.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4:4.8.6+git64-g5dc8b2b+dfsg-3+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets qt4-x11.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1786.data"
# $Id: $
