#use wml::debian::translation-check translation="222fe2a8c3db7e4bec94b5d9a4a285666247753c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web
Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21201">CVE-2021-21201</a>

<p>Gengming Liu et Jianyu Chen ont découvert un problème d'utilisation de
mémoire après libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21202">CVE-2021-21202</a>

<p>David Erceg a découvert un problème d'utilisation de mémoire après
libération dans les extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21203">CVE-2021-21203</a>

<p>asnine a découvert un problème d'utilisation de mémoire après libération
dans Blink et WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21204">CVE-2021-21204</a>

<p>Tsai-Simek, Jeanette Ulloa et Emily Voigtlander ont découvert un
problème d'utilisation de mémoire après libération dans Blink et WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21205">CVE-2021-21205</a>

<p>Alison Huffman a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21207">CVE-2021-21207</a>

<p>koocola et Nan Wang ont découvert une utilisation de mémoire après
libération dans la base de données indexée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21208">CVE-2021-21208</a>

<p>Ahmed Elsobky a découvert une erreur de validation de données dans le
scanner de codes QR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21209">CVE-2021-21209</a>

<p>Tom Van Goethem a découvert une erreur d'implémentation dans l'API
Storage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21210">CVE-2021-21210</a>

<p>@bananabr a découvert une erreur dans l'implémentation du réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21211">CVE-2021-21211</a>

<p>Akash Labade a découvert une erreur dans l'implémentation de la
navigation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21212">CVE-2021-21212</a>

<p>Hugo Hue et Sze Yui Chau ont découvert une erreur dans l'interface
utilisateur de la configuration du réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21213">CVE-2021-21213</a>

<p>raven a découvert un problème d'utilisation de mémoire après libération
dans l'implémentation de WebMIDI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21214">CVE-2021-21214</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert
dans l'implémentation du réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21215">CVE-2021-21215</a>

<p>Abdulrahman Alqabandi a découvert une erreur dans la fonction Autofill.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21216">CVE-2021-21216</a>

<p>Abdulrahman Alqabandi a découvert une erreur dans la fonction Autofill.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21217">CVE-2021-21217</a>

<p>Zhou Aiting a découvert une utilisation de mémoire non initialisée dans
la bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21218">CVE-2021-21218</a>

<p>Zhou Aiting a découvert une utilisation de mémoire non initialisée dans
la bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21219">CVE-2021-21219</a>

<p>Zhou Aiting a découvert une utilisation de mémoire non initialisée dans
la bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21221">CVE-2021-21221</a>

<p>Guang Gong a découvert une validation insuffisante des entrées non
fiables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21222">CVE-2021-21222</a>

<p>Guang Gong a découvert un problème de dépassement de tampon dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21223">CVE-2021-21223</a>

<p>Guang Gong a découvert un problème de dépassement d'entier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21224">CVE-2021-21224</a>

<p>Jose Martinez a découvert une erreur de type dans la bibliothèque
JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21225">CVE-2021-21225</a>

<p>Brendon Tiszka a découvert un problème d'accès mémoire hors limites dans
la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21226">CVE-2021-21226</a>

<p>Brendon Tiszka a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation du réseau.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 90.0.4430.85-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4906.data"
# $Id: $
