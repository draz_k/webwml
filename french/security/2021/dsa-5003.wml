#use wml::debian::translation-check translation="fe8a2087097d88ffc91293a2c8530e551b714fce" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, un serveur de
fichier SMB/CIFS, d'impression et de connexion pour UNIX.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2124">CVE-2016-2124</a>

<p>Stefan Metzmacher a signalé que les connexions d'un client SMB1 peuvent
être déclassées vers une authentification en texte en clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25717">CVE-2020-25717</a>

<p>Andrew Bartlett a signalé que Samba peut faire correspondre les
utilisateurs du domaine à des utilisateurs locaux d'une manière non
désirée, permettant une élévation de privilèges. La mise à jour introduit
un nouveau paramètre <q>min domain uid</q> (1000 par défaut) pour ne pas
accepter un UID UNIX inférieur à cette valeur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25718">CVE-2020-25718</a>

<p>Andrew Bartlett a signalé que Samba comme serveur AD DC, lorsqu'il est
rejoint par un contrôleur de domaine en lecture seule (RODC), ne confirmait
pas si le RODC était autorisé à produire un ticket pour cet utilisateur,
lui permettant de produire des tickets d'administrateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25719">CVE-2020-25719</a>

<p>Andrew Bartlett a signalé que Samba comme serveur AD DC ne s'appuyait
pas sur des tickets SID et PAC dans Kerberos et pourrait être confus sur le
ticket présenté par un utilisateur. Si un compte privilégié était attaqué,
cela pourrait conduire à la compromission complète du domaine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25721">CVE-2020-25721</a>

<p>Andrew Bartlett a signalé que Samba comme serveur AD DC ne fournit pas
aux applications Linux de moyen pour obtenir de SID fiable (ni d'attribut
samAccountName) dans les tickets produits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25722">CVE-2020-25722</a>

<p>Andrew Bartlett a signalé que Samba comme serveur AD DC ne réalisait pas
une vérification suffisante d'accès et de conformité des données stockées,
permettant éventuellement la compromission complète du domaine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3738">CVE-2021-3738</a>

<p>William Ross a signalé que le serveur RPC AD DC de Samba peut utiliser
la mémoire qui a été libérée lors de la clôture d'une sous connexion, avec
pour conséquences un déni de service, et éventuellement, une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23192">CVE-2021-23192</a>

<p>Stefan Metzmacher a signalé que si le client d'un serveur Samba envoyait
une très grande requête DCE/RPC et choisissait de la fragmenter, un
attaquant pourrait remplacer les derniers fragments par leurs propres
données, contournant les exigences de signature.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2:4.13.13+dfsg-1~deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de samba, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5003.data"
# $Id: $
