#use wml::debian::translation-check translation="0cfee114b2d0c22d16e262ece0a0804aac60d237" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans la servlet Tomcat et
le moteur JSP. Elles pourraient avoir pour conséquence une attaque par
dissimulation de requête HTTP, l'exécution de code dans le connecteur AJP
(désactivé par défaut dans Debian) ou une attaque de type « homme du
milieu » à l'encontre de l'interface JMX.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 9.0.31-1~deb10u1. Le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">\
CVE-2020-1938</a> peut nécessiter des changements de configuration quand
Tomcat est utilisé avec le connecteur AJP, par exemple en combinaison avec
libapache-mod-jk. Par exemple, l'attribut <q>secretRequired</q> est réglé
à <q>true</q> par défaut désormais. Pour les configurations qui sont
affectées, il est recommandé d'examiner
<a href="https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html">\
https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html</a>
avant de déployer la mise à jour.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4680.data"
# $Id: $
