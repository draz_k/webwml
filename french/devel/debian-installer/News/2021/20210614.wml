#use wml::debian::translation-check translation="d210742b3acd9312bad122e0c31e48d8b4906c95" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bullseye RC 2</define-tag>
<define-tag release_date>2021-06-14</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la deuxième version candidate pour Debian 11 <q>Bullseye</q>.
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>cdebconf :
    <ul>
      <li>interception de l'événement <q>demande de dimension</q> et
        ajustement de la largeur demandée à 300 pixels minimum. Cela semble
        suffisant pour éviter une boucle infinie dans GTK
        (<a href="https://bugs.debian.org/988786">nº 988786</a>).
        Cela corrige plusieurs scénarios dont :
        <ul>
          <li>blocage avant la disponibilité de l'interpréteur en mode de
            récupération (<a href="https://bugs.debian.org/987377">nº 987377</a>) ;</li>
          <li>blocage immédiat avec le singhalais et d'autres langues
            (<a href="https://bugs.debian.org/987449">nº 987449</a>) ;</li>
          <li>blocage à diverses étapes en suédois (sélection de miroir ou
            configuration du gestionnaire de paquets).</li>
        </ul>
      </li>
      <li>capture des messages de journalisation structurés du nouveau style
        de GLib (<a href="https://bugs.debian.org/988589">nº 988589</a>) ;</li>
      <li>alignement à droite de l'affichage des messages d'information (par
        exemple <q>Rescue mode</q>) rétabli dans la mesure où le nom et le
        logo Debian sont à nouveau sur le côté gauche avec le thème
        Homeworld ;</li>
      <li>assurance que les messages d'information (par exemple <q>mode de
        récupération</q>) sont affichés sur la bannière de l'écran de
        démarrage (<a href="https://bugs.debian.org/882804">nº 882804</a>).</li>
    </ul>
  </li>
  <li>choose-mirror :
    <ul>
      <li>mise à jour de Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>debian-cd :
    <ul>
      <li>Ajout de brltty et espeakup à toutes les images à partir de
        l'image d'installation par le réseau, pour les utilisateurs qui font
        l'installation sans un miroir de réseau (<a href="https://bugs.debian.org/678065">nº 678065</a>).</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>passage de l'ABI du noyau Linux à la version 5.10.0-7.</li>
    </ul>
  </li>
  <li>grub-installer :
    <ul>
      <li>assurance que la réinstallation de GRUB sur les systèmes avec BIOS
        ne rapporte pas une erreur quand tout s'est bien passé (<a href="https://bugs.debian.org/988826">nº 988826</a>).</li>
    </ul>
  </li>
  <li>gtk+2.0 :
    <ul>
      <li>boucle de réaffichage susceptible de se produire dans
        l'installateur graphique évitée (<a href="https://bugs.debian.org/988786">nº 988786</a>).</li>
    </ul>
  </li>
  <li>lowmem :
    <ul>
      <li>forçage des niveaux de lowmem autorisé (par exemple lowmem=+0) ;</li>
      <li>mise à jour des niveaux de lowmem pour arm64, armhf, mipsel,
        mips64el et ppc64el.</li>
    </ul>
  </li>
  <li>open-iscsi :
    <ul>
      <li>transfert du contenu du paquet de la bibliothèque dans le paquet
        udeb, plutôt que d'avoir le paquet udeb dépendant du paquet de la
        bibliothèque (<a href="https://bugs.debian.org/987568">nº 987568</a>).</li>
    </ul>
  </li>
  <li>partman-base :
    <ul>
      <li>correction et prise en charge étendue pour les périphériques
        /dev/wd* avec rumpdisk.</li>
    </ul>
  </li>
  <li>systemd :
    <ul>
      <li>udev-udeb : configuration des liens symboliques /dev/fd,
        /dev/std{in,out,err} (<a href="https://bugs.debian.org/975018">nº 975018</a>).</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>linux :
    <ul>
      <li>[arm64] udeb : inclusion du module mdio pour RPi4 Ethernet (<a href="https://bugs.debian.org/985956">nº 985956</a>) ;</li>
      <li>[s390x] udeb : inclusion des modules scsi standards fournissant le
        module virtio_blk (<a href="https://bugs.debian.org/988005">nº 988005</a>).</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>78 langues sont prises en charge dans cette version.</li>
  <li>La traduction est complète pour 33 de ces langues.</li>
</ul>



<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
