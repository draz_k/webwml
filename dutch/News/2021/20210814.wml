#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff"
<define-tag pagetitle>Debian 11 <q>bullseye</q> uitgebracht</define-tag>
<define-tag release_date>14-08-2021</define-tag>
#use wml::debian::news


<p>Na 2 jaar, 1 maand en 9 dagen ontwikkeling presenteert het Debian-project
met trots zijn nieuwe stabiele versie 11 (codenaam <q>bullseye</q>), die de
komende 5 jaar zal worden ondersteund dankzij het gezamenlijke werk van het
<a href="https://security-team.debian.org/">Debian beveiligingsteam</a> en het
<a href="https://wiki.debian.org/LTS">Debian team voor langetermijnondersteuning</a>.
</p>

<p>
Debian 11 <q>bullseye</q> wordt geleverd met verschillende desktoptoepassingen
en -omgevingen. Het bevat nu onder andere de grafische werkomgevingen:
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>


<p>Deze release bevat meer dan 11.294 nieuwe pakketten voor een totaal aantal
van 59.551 pakketten, samen met een aanzienlijke vermindering van meer dan
9.519 pakketten die als <q>verouderd</q> werden gemarkeerd en verwijderd.
42.821 pakketten werden bijgewerkt en 5.434 pakketten bleven ongewijzigd.
</p>

<p>
<q>bullseye</q> is onze eerste uitgave die een Linux kernel biedt met
ondersteuning voor het exFAT bestandssysteem en deze standaard gebruikt om
exFAT bestandssystemen aan te koppelen. Bijgevolg is het niet langer nodig om
de implementatie te gebruiken van een bestandssysteem-in-gebruikersruimte welke
via het pakket exfat-fuse wordt aangeboden. Gereedschappen om een exFAT
bestandssysteem aan te maken en te controleren worden verschaft in het pakket
exfatprogs.
</p>


<p>
De meeste moderne printers zijn in staat om stuurprogrammaloos te printen en te
scannen, zonder dat er leveranciersspecifieke (vaak niet-vrije)
stuurprogramma's nodig zijn.
<q>bullseye</q> introduceert een nieuw pakket, ipp-usb, dat gebruik maakt van
het leveranciersneutrale protocol IPP-over-USB dat door veel moderne printers
wordt ondersteund. Hierdoor kan een USB-apparaat worden behandeld als een
netwerkapparaat. De officiële stuurprogrammaloze SANE-backend wordt geleverd
door sane-escl in libsane1, dat gebruik maakt van het eSCL-protocol.
</p>

<p>
Systemd in <q>bullseye</q> activeert standaard de permanente
journaalfunctionaliteit met een impliciete terugval naar vluchtige opslag. Dit
stelt gebruikers die niet afhankelijk zijn van speciale functies, in staat om
traditionele logboekachtergronddiensten te verwijderen en over te schakelen op
het gebruik van enkel het systemd-journaal.
</p>

<p>
Het team Debian Med heeft deelgenomen aan de strijd tegen COVID-19 door
software te verpakken voor het onderzoek van het virus op sequentieniveau en
voor de bestrijding van de pandemie met de instrumenten die gebruikt worden in
de epidemiologie; dit werk zal worden voortgezet met de nadruk op hulpmiddelen
voor machinaal leren voor de beide domeinen. Het werk van het team met het team
voor kwaliteitsverzekering en voor permanente integratie is van cruciaal belang
voor het bereiken van consistente reproduceerbare resultaten, wat in de
wetenschappen vereist wordt.
De specifieke uitgave Debian Med heeft een reeks prestatiekritieke toepassingen
die nu profiteren van SIMD Everywhere. Om pakketten te installeren die worden
onderhouden door het Debian Med-team, installeert u de metapakketten met de
naam med-*, die versie 3.6.x hebben.
</p>

<p>
Chinees, Japans, Koreaans en vele andere talen hebben nu een nieuwe
Fcitx5-invoermethode, die de opvolger is van de populaire Fcitx4
in <q>buster</q>; deze nieuwe versie heeft een veel betere ondersteuning voor
uitbreidingen voor Wayland (de standaard beeldschermbeheerder).
</p>

<p>
Debian 11 <q>bullseye</q> bevat talrijke bijgewerkte softwarepakketten (meer
dan 72% van alle pakketten uit de vorige release), zoals:
</p>
<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Linux kernel 5.10 serie</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>meer dan 59.000 andere gebruiksklare softwarepakketten, gebouwd uit meer
dan 30.000 broncodepakketten.</li>
</ul>

<p>
Met deze brede selectie van pakketten en zijn traditionele brede
architectuurondersteuning, blijft Debian opnieuw trouw aan zijn doel om het
<q>Universele Besturingssysteem</q> te zijn. Het is geschikt voor veel
verschillende gebruikssituaties: van desktopsystemen tot netbooks; van
ontwikkelingsservers tot clustersystemen; en voor database-, web- en
opslagservers. Tegelijkertijd zorgen extra inspanningen voor
kwaliteitsgarantie, zoals automatische installatie- en opwaarderingstests voor
alle pakketten uit het Debian-archief, ervoor dat <q>bullseye</q> voldoet aan
de hoge verwachtingen die gebruikers hebben van een stabiele Debian-release.
</p>

<p>
Een totaal van negen architecturen wordt ondersteund:
64-bits PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bits PC / Intel IA-32 (<code>i386</code>),
64-bits little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bits IBM S/390 (<code>s390x</code>),
voor ARM, <code>armel</code>
en <code>armhf</code> voor oudere en meer recente 32-bits hardware,
plus <code>arm64</code> voor de 64-bits <q>AArch64</q>-architectuur,
en voor MIPS, <code>mipsel</code> (little-endian)-architecturen voor 32-bits hardware
en <code>mips64el</code>-architectuur voor 64-bits little-endian hardware.
</p>

<h3>Wilt u het eens uitproberen?</h3>
<p>
Als u gewoon Debian 11 <q>bullseye</q> wilt uitproberen zonder het te
installeren, kunt u een van de beschikbare
<a href="$(HOME)/CD/live/">live-images</a> gebruiken die via het geheugen van
uw computer het volledige besturingssysteem laden en uitvoeren in een
alleen-lezen modus.
</p>

<p>
Deze live-images worden geleverd voor de <code>amd64</code>- en de
<code>i386</code>-architectuur en zijn beschikbaar voor dvd's, USB-sticks en
netboot-opstellingen. De gebruiker kan kiezen uit verschillende grafische
werkomgevingen om uit te proberen: GNOME, KDE Plasma, LXDE, LXQt, MATE, en Xfce.
Debian Live <q>bullseye</q> heeft een standaard live-image, waardoor het ook
mogelijk is om een basis Debian-systeem uit te proberen zonder een van de
grafische gebruikersinterfaces.
</p>

<p>
Als u het besturingssysteem leuk vindt, heeft u de mogelijkheid om het vanaf
het live-image op de harde schijf van uw computer te installeren. Het
live-image bevat zowel het onafhankelijke Calamares-installatieprogramma als
het standaard Debian-installatieprogramma.
Meer informatie is te vinden in de
<a href="$(HOME)/releases/bullseye/releasenotes">notities bij de release</a>
en de secties van de website van Debian over
<a href="$(HOME)/CD/live/">live installatie-images</a>.
</p>

<p>
Om Debian 11 <q>bullseye</q> rechtstreeks op de harde schijf van uw computer te
installeren, kunt u kiezen uit verschillende installatiemedia zoals
Blu-rayschijf, dvd, cd, USB-stick of via een netwerkverbinding. Verschillende
grafische werkomgevingen &mdash; Cinnamon, GNOME, KDE Plasma Desktop en
Applicaties, LXDE, LXQt, MATE en Xfce &mdash; kunnen via deze images worden
geïnstalleerd. Daarnaast zijn er <q>multi-architectuur</q>-cd's beschikbaar die
installatie ondersteunen van een keuze aan architecturen vanaf een enkele
schijf. Of u kunt altijd opstartbare USB-installatiemedia maken (zie de
<a href="$(HOME)/releases/bullseye/installmanual">Installatiehandleiding</a>
voor meer details).
</p>

# Translators: some text taken from /devel/debian-installer/News/2021/20210802

<p>
Er is hard gewerkt aan de ontwikkeling aan het Debian-installatieprogramma, wat
heeft geleid tot verbeterde hardware-ondersteuning en andere nieuwe functies.
</p>
<p>
In sommige gevallen kan een succesvolle installatie nog steeds schermproblemen
geven bij het herstarten in het geïnstalleerde systeem; voor die gevallen zijn
er <a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">een paar tijdelijke oplossingen</a> die kunnen helpen om toch
in te loggen.
Er is ook een <a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">op isenkram gebaseerde procedure</a>
waarmee gebruikers op een geautomatiseerde manier ontbrekende firmware op hun
systeem kunnen opsporen en oplossen. Natuurlijk moet men de voor- en nadelen
van het gebruik van dit gereedschap afwegen, aangezien het zeer waarschijnlijk
is dat men niet-vrije pakketten zal moeten installeren.</p>

<p>
Daarnaast zijn de <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">niet-vrije installatie-images die firmwarepakketten
bevatten</a> verbeterd, zodat ze kunnen anticiperen op de behoefte aan firmware
in het geïnstalleerde systeem (bijv. firmware voor grafische
kaarten van AMD of Nvidia, of nieuwere generaties van audio hardware van Intel).
</p>

<p>
Voor cloud-gebruikers biedt Debian directe ondersteuning voor veel van de
bekendste cloud-platforms. Officiële Debian-images zijn eenvoudig te selecteren
via elke image-marktplaats. Debian publiceert ook
<a href="https://cloud.debian.org/images/openstack/current/">vooraf gebouwde
OpenStack-images</a> voor de architecturen <code>amd64</code> en
<code>arm64</code>, klaar om te downloaden en te gebruiken in lokale
cloudopstellingen.
</p>

<p>
Debian kan nu in 76 talen worden geïnstalleerd, en de meeste daarvan zijn
beschikbaar in zowel een tekstgebaseerde als een grafische gebruikersinterface.
</p>

<p>
De installatie-images kunnen nu worden gedownload via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (de aanbevolen methode),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> en
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; zie
<a href="$(HOME)/CD/">Debian op cd's</a> voor verdere informatie.
<q>bullseye</q> zal weldra ook beschikbaar zijn op fysieke dcd-, cd-, en
blu-rayschijven bij talloze <a href="$(HOME)/CD/vendors">leveranciers</a>.
</p>


<h3>Debian opwaarderen</h3>
<p>
Opwaarderingen naar Debian 11 vanaf de vorige release, Debian 10 (codenaam
<q>buster</q>) worden voor de meeste configuraties automatisch afgehandeld door
het pakketbeheerprogramma APT.
</p>

<p>
Voor bullseye heet de beveiligingssuite nu bullseye-security en gebruikers
moeten hun sources-list-bestanden van APT dienovereenkomstig aanpassen bij het
opwaarderen. Als uw APT-configuratie ook pinning of
<code>APT::Default-Release</code> omvat, is het waarschijnlijk dat er ook
aanpassingen nodig zullen zijn. Zie de sectie
<a href="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive">Gewijzigde indeling van het
beveiligingsarchief</a> uit de notities bij de release voor meer details.
</p>

<p>
Als u vanop afstand opwaardeert, let dan op de sectie
<a href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">Geen nieuwe SSH-verbindingen mogelijk tijdens de opwaardering</a>.
</p>

<p>
Zoals altijd kunnen Debian-systemen moeiteloos worden opgewaardeerd, zonder
enige gedwongen uitvaltijd, maar het wordt sterk aanbevolen om de
<a href="$(HOME)/releases/bullseye/releasenotes">notities bij de release</a>
te lezen evenals de
<a href="$(HOME)/releases/bullseye/installmanual">installatiehandleiding</a>
voor mogelijke problemen, en voor gedetailleerde instructies over installeren
en opwaarderen. De notities bij de release zullen in de weken na de release
verder worden verbeterd en vertaald naar andere talen.
</p>


<h2>Over Debian</h2>

<p>
Debian is een vrij besturingssysteem dat ontwikkeld wordt door duizenden
vrijwilligers van over de hele wereld die samenwerken via het internet. De
belangrijkste sterke punten van het Debian-project zijn het feit dat het
gedragen wordt door vrijwilligers, de toewijding aan het Debian Sociale
Contract en aan Vrije Software, en het streven om het best mogelijke
besturingssysteem aan te bieden. Deze nieuwe release is weer een belangrijke
stap in die richting.
</p>


<h2>Contactinformatie</h2>

<p>
Bezoek voor meer informatie de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a> of stuur een e-mail naar
&lt;press@debian.org&gt;.
</p>
