#use wml::debian::blends::hamradio

{#alternate_navbar#:
   <div id="second-nav">
      <p><a href="$(HOME)/blends/hamradio/">Debian&nbsp;voor&nbsp;amateurradio</a></p>
    <ul>
      <li><a href="$(HOME)/blends/hamradio/about">Over&nbsp;de&nbsp;uitgave</a></li>
      <li><a href="$(HOME)/blends/hamradio/News/">Nieuwsarchieven</a></li>
      <li><a href="$(HOME)/blends/hamradio/contact">Contact</a></li>
      <li><a href="$(HOME)/blends/hamradio/get/">De&nbsp;uitgave&nbsp;verkrijgen</a>
         <ul>
         <li><a href="$(HOME)/blends/hamradio/get/live">Live-images&nbsp;downloaden</a></li>
         <li><a href="$(HOME)/blends/hamradio/get/metapackages">Metapakketten&nbsp;gebruiken</a></li>
	 </ul>
      </li>
      <li><a href="$(HOME)/blends/hamradio/docs/">Documentatie</a>
        <ul>
        <li><a href="<hamradio-handbook-html/>">Amateurradiohandleiding</a></li>
        </ul>
      </li>
      <li><a href="$(HOME)/blends/hamradio/support">Ondersteuning</a></li>
      <li><a href="$(HOME)/blends/hamradio/dev">Ontwikkeling</a>
        <ul>
        <li><a href="https://wiki.debian.org/DebianHams">Ontwikkelaarsteam</a></li>
        <li><a href="<hamradio-maintguide-html/>">Ontwikkelaarshandleiding</a></li>
        </ul>
      </li>
      <li><a href="https://twitter.com/DebianHamradio"><img src="$(HOME)/blends/hamradio/Pics/twitter.gif" alt="Twitter" width="80" height="15"></a></li>
    </ul>
   </div>
:#alternate_navbar#}
