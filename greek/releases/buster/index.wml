#use wml::debian::template title="Πληροφορίες κυκλοφορίας της έκδοσης Debian &ldquo;buster&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="2b2b2d98876137a0efdabdfc2abad6088d4c511f" maintainer="galaxico"

<p>Το Debian <current_release_buster>
κυκλοφόρησε στις <a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "Το Debian 10.0 κυκλοφόρησε αρχικά στις <:=spokendate('2019-07-06'):>."
/>
Η κυκλοφορία περιλαμβάνει αρκετές σημαντικές 
αλλαγές, που περιγράφονται στο 
<a href="$(HOME)/News/2019/20190706">Δελτίο τύπου</a> και 
στις  <a href="releasenotes">Σημειώσεις της έκδοσης</a>.</p>

<p><strong>Το Debian 10 το διαδέχτηκε το
<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('xxxx-xx-xx'):>.
</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>However, buster benefits from Long Term Support (LTS) until
#the end of xxxxx 20xx. The LTS is limited to i386, amd64, armel, armhf and arm64.
#All other architectures are no longer supported in buster.
#For more information, please refer to the <a
#href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
#</strong></p>

<p>Για να αποκτήσετε και να εγκαταστήσετε το Debian, δείτε
τη σελίδα <a href="debian-installer/">πληροφορίες εγκατάστασης</a> και
τον <a href="installmanual">Οδηγό Εγκατάστασης</a>. Για να κάνετε αναβάθμιση
από μια παλιότερη έκδοση του Debian, δείτε τις οδηγίες στις 
<a href="releasenotes">Σημειώσεις της έκδοσης</a>.</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Αυτή η έκδοση υποστηρίζει τις παρακάτων αρχιτεκτονικές υπολογιστών:</p>
# <p>Computer architectures supported at initial release of buster:</p> ### Use this line when LTS starts, instead of the one above.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>


<p>Παρά τις επιθυμίες μας, μπορεί να υπάρχουν μερικά προβλήματα στην παρούσα 
έκδοση αν και έχει ανακοινωνθεί ως <em>σταθερή</em>. 
Έχουμε κάνει
<a href="errata">μια λίστα με τα σημαντικότερα γνωστά προβλήματα</a>, και 
μπορείτε πάντα να
<a href="reportingbugs">αναφέρετε οποιαδήποτε άλλα</a> σε μας.</p>


<p>Τέλος, αλλά εξίσου σημαντικό, έχουμε δημιουργήσει μια λίστα των <a 
href="credits">ατόμων που αξίζουν τα εύσημα</a> για την πραγματοποίηση
της κυκλοφορίας αυτής της έκδοσης.</p>
