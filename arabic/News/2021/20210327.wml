#use wml::debian::translation-check translation="2a3fa2ac1b49f491737f3c89828986a43c00a61e" maintainer="Med"

<define-tag pagetitle>تحديث دبيان 10 : الإصدار 10.9</define-tag>
<define-tag release_date>2021-03-27</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
يُسعد مشروع دبيان الإعلان عن التحديث الثامن لتوزيعته المستقرة دبيان <release> (الاسم الرمزي <q><codename></q>).
بالإضافة إلى تسوية بعض المشكلات الحرجة يُصلح هذا التحديث بالأساس مشاكلات الأمان. تنبيهات الأمان أُعلنت بشكل منفصِل وفقط مُشار إليها في هذا الإعلان.
</p>

<p>
يُرجى ملاحظة أن هذا التحديث لا يُشكّل إصدار جديد لدبيان 10 بل فقط تحديثات لبعض الحُزم المُضمّنة
وبالتالي ليس بالضرورة رميُ الوسائط القديمة للإصدار <q><codename></q>، يمكن تحديث الحُزم باستخدام مرآة دبيان مُحدّثة.
</p>

<p>
الذين يُثبّتون التحديثات من security.debian.org باستمرار لن يكون عليهم تحديث العديد من الحُزم،
أغلب التحديثات مُضمّنة في هذا التحديث.
</p>

<p>
صور جديدة لأقراص التثبيت ستكون متوفرة في موضِعها المعتاد.
</p>

<p>
يمكن الترقية من  تثبيت آنِيّ إلى هذه المراجعة بتوجيه نظام إدارة الحُزم إلى إحدى مرايا HTTP الخاصة بدبيان.
قائمة شاملة لمرايا دبيان على المسار :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>إصلاح العديد من العِلاّت</h2>

<p>هذا التحديث للإصدار المستقر أضاف بعض الإصلاحات المهمة للحُزم التالية :</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السّبب</th></tr>
<correction avahi "Remove avahi-daemon-check-dns mechanism, which is no longer needed">
<correction base-files "Update /etc/debian_version for the 10.9 point release">
<correction cloud-init "Avoid logging generated passwords to world-readable log files [CVE-2021-3429]">
<correction debian-archive-keyring "Add bullseye keys; retire jessie keys">
<correction debian-installer "Use 4.19.0-16 Linux kernel ABI">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction exim4 "Fix use of concurrent TLS connections under GnuTLS; fix TLS certificate verification with CNAMEs; README.Debian: document the limitation/extent of server certificate verification in the default configuration">
<correction fetchmail "No longer report <q>System error during SSL_connect(): Success</q>; remove OpenSSL version check">
<correction fwupd "Add SBAT support">
<correction fwupd-amd64-signed "Add SBAT support">
<correction fwupd-arm64-signed "Add SBAT support">
<correction fwupd-armhf-signed "Add SBAT support">
<correction fwupd-i386-signed "Add SBAT support">
<correction fwupdate "Add SBAT support">
<correction fwupdate-amd64-signed "Add SBAT support">
<correction fwupdate-arm64-signed "Add SBAT support">
<correction fwupdate-armhf-signed "Add SBAT support">
<correction fwupdate-i386-signed "Add SBAT support">
<correction gdnsd "Fix stack overflow with overly-large IPv6 addresses [CVE-2019-13952]">
<correction groff "Rebuild against ghostscript 9.27">
<correction hwloc-contrib "Enable support for the ppc64el architecture">
<correction intel-microcode "Update various microcode">
<correction iputils "Fix ping rounding errors; fix tracepath target corruption">
<correction jquery "Fix untrusted code execution vulnerabilities [CVE-2020-11022 CVE-2020-11023]">
<correction libbsd "Fix out-of-bounds read issue [CVE-2019-20367]">
<correction libpano13 "Fix format string vulnerability">
<correction libreoffice "Do not load encodings.py from current directoy">
<correction linux "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction linux-latest "Update to -15 kernel ABI; update for -16 kernel ABI">
<correction linux-signed-amd64 "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction linux-signed-arm64 "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction linux-signed-i386 "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction lirc "Normalize embedded ${DEB_HOST_MULTIARCH} value in /etc/lirc/lirc_options.conf to find unmodified configuration files on all architectures; recommend gir1.2-vte-2.91 instead of non-existent gir1.2-vte">
<correction m2crypto "Fix test failure with recent OpenSSL versions">
<correction openafs "Fix outgoing connections after unix epoch time 0x60000000 (14 January 2021)">
<correction portaudio19 "Handle EPIPE from alsa_snd_pcm_poll_descriptors, fixing crash">
<correction postgresql-11 "New upstream stable release; fix information leakage in constraint-violation error messages [CVE-2021-3393]; fix CREATE INDEX CONCURRENTLY to wait for concurrent prepared transactions">
<correction privoxy "Security issues [CVE-2020-35502 CVE-2021-20209 CVE-2021-20210 CVE-2021-20211 CVE-2021-20212 CVE-2021-20213 CVE-2021-20214 CVE-2021-20215 CVE-2021-20216 CVE-2021-20217 CVE-2021-20272 CVE-2021-20273 CVE-2021-20275 CVE-2021-20276]">
<correction python3.7 "Fix CRLF injection in http.client [CVE-2020-26116]; fix buffer overflow in PyCArg_repr in _ctypes/callproc.c [CVE-2021-3177]">
<correction redis "Fix a series of integer overflow issues on 32-bit systems [CVE-2021-21309]">
<correction ruby-mechanize "Fix command injection issue [CVE-2021-21289]">
<correction systemd "core: make sure to restore the control command id, too, fixing a segfault; seccomp: allow turning off of seccomp filtering via an environment variable">
<correction uim "libuim-data: Perform symlink_to_dir conversion of /usr/share/doc/libuim-data in the resurrected package for clean upgrades from stretch">
<correction xcftools "Fix integer overflow vulnerability [CVE-2019-5086 CVE-2019-5087]">
<correction xterm "Correct upper-limit for selection buffer, accounting for combining characters [CVE-2021-27135]">
</table>


<h2>تحديثات الأمان</h2>


<p>
أضافت هذه المراجعة تحديثات الأمان التالية للإصدار المستقر.
سبَق لفريق الأمان نشر تنبيه لكل تحديث :
</p>

<table border=0>
<tr><th>مُعرَّف التنبيه</th>  <th>الحزمة</th></tr>
<dsa 2021 4826 nodejs>
<dsa 2021 4844 dnsmasq>
<dsa 2021 4845 openldap>
<dsa 2021 4846 chromium>
<dsa 2021 4847 connman>
<dsa 2021 4849 firejail>
<dsa 2021 4850 libzstd>
<dsa 2021 4851 subversion>
<dsa 2021 4853 spip>
<dsa 2021 4854 webkit2gtk>
<dsa 2021 4855 openssl>
<dsa 2021 4856 php7.3>
<dsa 2021 4857 bind9>
<dsa 2021 4858 chromium>
<dsa 2021 4859 libzstd>
<dsa 2021 4860 openldap>
<dsa 2021 4861 screen>
<dsa 2021 4862 firefox-esr>
<dsa 2021 4863 nodejs>
<dsa 2021 4864 python-aiohttp>
<dsa 2021 4865 docker.io>
<dsa 2021 4867 grub-efi-amd64-signed>
<dsa 2021 4867 grub-efi-arm64-signed>
<dsa 2021 4867 grub-efi-ia32-signed>
<dsa 2021 4867 grub2>
<dsa 2021 4868 flatpak>
<dsa 2021 4869 tiff>
<dsa 2021 4870 pygments>
<dsa 2021 4871 tor>
<dsa 2021 4872 shibboleth-sp>
</table>



<h2>مُثبِّت دبيان</h2>
<p>
حُدِّث المُثبِّت ليتضمن الإصلاحات المندرجة في هذا الإصدار المستقر.
</p>

<h2>المسارات</h2>

<p>
القائمة الكاملة للحُزم المُغيّرة في هذه المراجعة :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>التوزيعة المستقرة الحالية :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>التحديثات المقترحة للتوزيعة المستقرة :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>معلومات حول التوزيعة المستقرة (ملاحظات الإصدار والأخطاء إلخ) :</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>معلومات وإعلانات الأمان :</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>حول دبيان</h2>

<p>
مشروع دبيان هو اتحاد لمطوري البرمجيات الحُرّة تطوعوا بالوقت والمجهود لإنتاج نظام تشعيل دبيان حُر بالكامل.
</p>

<h2>معلومات الاتصال</h2>

<p>
لمزيد من المعلومات يُرجى زيارة موقع دبيان
<a href="$(HOME)/">https://www.debian.org/</a>
أو الاتصال بفريق إصدار المستقرة على
&lt;debian-release@lists.debian.org&gt;.</p>

