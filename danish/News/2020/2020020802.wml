#use wml::debian::translation-check translation="2913230d58de12a2d0daeab2fd1f532a65fa5c2a"
<define-tag pagetitle>Opdateret Debian 9: 9.12 udgivet</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den tolvte opdatering af 
dets gamle stabile distribution, Debian <release> (kodenavn <q><codename></q>).  
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer, sammen med 
nogle få rettelser af alvorlige problemer.  Sikkerhedsbulletiner er allerede 
udgivet separat og der vil blive refereret til dem, hvor de er tilgængelige.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Online-opdatering til denne revision gøres normalt ved at lade 
pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den gamle stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>					<th>Årsag</th></tr>
<correction base-files 					"Opdaterer til denne punktudgivelse">
<correction cargo 					"Ny opstrømsversion, for at understøtte Firefox ESR-tilbageførsler; retter bootstrap for armhf">
<correction clamav 					"Ny opstrømsudgave; retter problem med lammelsesangreb [CVE-2019-15961]; fjerner valgmuligheden ScanOnAccess option, som erstattes med clamonacc">
<correction cups 					"Retter validering af standardsprog i ippSetValuetag [CVE-2019-2228]">
<correction debian-installer 				"Genopbygger mod oldstable-proposed-updates; opsætter også gfxpayload=keep i undermenuer for at rette ulæselige skrifttyper på hidpi-skærme i netbootaftryk startet med EFI; opdaterer USE_UDEBS_FROM's standard fra unstable til stretch, for at hjælpe brugere med ud foretage lokale opbygninger">
<correction debian-installer-netboot-images 		"Genopbygger mod stretch-proposed-updates">
<correction debian-security-support 			"Opdaterer flere pakkers sikkerhedssupportstatus">
<correction dehydrated 					"Ny opstrømsudgave; anvender som standard ACMEv2-API">
<correction dispmua 					"Ny opstrømsudgave kompatibel med Thunderbird 68">
<correction dpdk 					"Ny stabil opstrømsudgave; retter vhost-regression opstået ved rettelsen af CVE-2019-14818">
<correction fence-agents 				"Retter ufuldstændig fjernelse af fence_amt_ws">
<correction fig2dev 					"Tillader Fig v2-tekststrenge som slutter med flere ^A [CVE-2019-19555]">
<correction flightcrew 					"Sikkerhedsrettelser [CVE-2019-13032 CVE-2019-13241]">
<correction freetype 					"Håndter på korrekt vis deltaer i TrueType GX-skrifttyper, retter rendering af variable hint'ede skrifttyper i Chromium og Firefox">
<correction glib2.0 					"Sikrer at libdbus-klienter kan autentificere med en GDBusServer som den i ibus">
<correction gnustep-base 				"Retter sårbarhed i forbindelse med UDP-forstærkelse">
<correction italc 					"Sikkerhedsrettelser [CVE-2018-15126 CVE-2018-15127 CVE-2018-20019 CVE-2018-20020 CVE-2018-20021 CVE-2018-20022 CVE-2018-20023 CVE-2018-20024 CVE-2018-20748 CVE-2018-20749 CVE-2018-20750 CVE-2018-6307 CVE-2018-7225 CVE-2019-15681]">
<correction libdate-holidays-de-perl 			"Markerer den internationale børnedag (20. september) som en fridag i Thüringen fra 2019 og frem">
<correction libdatetime-timezone-perl 			"Opdaterer medfølgende data">
<correction libidn 					"Retter sårbarhed i forbindelse med lammelsesangreb i håndtering af Punycode [CVE-2017-14062]">
<correction libjaxen-java 				"Retter opbygningsfejl ved at tillade testfejl">
<correction libofx 					"Retter problem med NULL-pointerdereference [CVE-2019-9656]">
<correction libole-storage-lite-perl 			"Retter fortolkning af år fra 2020 og frem">
<correction libparse-win32registry-perl 		"Retter fortolkning af år fra 2020 og frem">
<correction libperl4-corelibs-perl 			"Retter fortolkning af år fra 2020 og frem">
<correction libpst 					"Retter genkendelse af get_current_dir_name og trunkering af return">
<correction libsixel 					"Retter flere sikkerhedsproblemer [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libsolv 					"Retter heapbufferoverløb [CVE-2019-20387]">
<correction libtest-mocktime-perl 			"Retter fortolkning af år fra 2020 og frem">
<correction libtimedate-perl 				"Retter fortolkning af år fra 2020 og frem">
<correction libvncserver 				"RFBserver: Læk ikke stakhukommelse til remote [CVE-2019-15681]; løs en frysning under lukning af forbindelse og en segementeringsfejl ved flertrådede VNC-servere; retter problem med forbindelse til VMWare-servere; retter nedbrud i x11vnc når vncviewer forbinder sig">
<correction libxslt 					"Retter dinglende pointer i xsltCopyText [CVE-2019-18197]">
<correction limnoria 					"Retter fjern informationsafsløring og muligvis fjernudførelse af kode i Math-plugin'en [CVE-2019-19010]">
<correction linux 					"Ny stabil opstrømsudgave">
<correction linux-latest 				"Opdatering til Linux-kerne-ABI 4.9.0-12">
<correction llvm-toolchain-7 				"Deaktiverer gold-linkeren fra s390x; bootstrap med -fno-addrsig, stretchs binutils fungerer ikke med den på mips64el">
<correction mariadb-10.1 				"Ny stabil opstrømsudgave [CVE-2019-2974 CVE-2020-2574]">
<correction monit 					"Implementerer position uafhængig af CSRF-cookieværdi">
<correction node-fstream 				"Tværer et Link ud hvis det er i vejen for en File [CVE-2019-13173]">
<correction node-mixin-deep 				"Retter prototypeforurening [CVE-2018-3719 CVE-2019-10746]">
<correction nodejs-mozilla 				"Ny pakke til understøttelse af Firefox ESR-tilbageførsler">
<correction nvidia-graphics-drivers-legacy-340xx 	"Ny stabil opstrømsudgave">
<correction nyancat 					"Genopbygger i et rent miljø for at tilføje systemd-unit'et for nyancat-server">
<correction openjpeg2 					"Retter heapoverløb [CVE-2018-21010], heltalsoverløb [CVE-2018-20847] og division med nul [CVE-2016-9112]">
<correction perl 					"Retter fortolkning af år fra 2020 og frem">
<correction php-horde 					"Retter gemt problem med udførelse af skripter på tværs af servere i Horde Cloud Block [CVE-2019-12095]">
<correction postfix 					"Ny stabil opstrømsudgave; omgår dårlig TCP-loopbackperformance">
<correction postgresql-9.6 				"Ny opstrømsudgave">
<correction proftpd-dfsg 				"Retter NULL-pointerdereference i CRL-kontroller [CVE-2019-19269]">
<correction pykaraoke 					"Retter sti til skrifttyper">
<correction python-acme 				"Skifter til protokollen POST-as-GET">
<correction python-cryptography 			"Retter testsuitefejl når opbygget mod nyere versioner af OpenSSL">
<correction python-flask-rdf 				"Retter manglende afhængigheder i python3-flask-rdf">
<correction python-pgmagick 				"Håndterer versionsgenkendelse af graphicsmagick-sikkerhedsopdateringer som identificerer sig selv som version 1.4">
<correction python-werkzeug 				"Sikrer at Docker-containere har unikke debugger-PIN's [CVE-2019-14806]">
<correction ros-ros-comm 				"Retter problem med bufferoverløb [CVE-2019-13566]; retter heltalsoverløb [CVE-2019-13445]">
<correction ruby-encryptor 				"Ignorerer testfejl, retter opbygningsfejl">
<correction rust-cbindgen 				"Ny pakke til understøttelse af Firefox ESR-tilbageførsler">
<correction rustc 					"Ny opstrømsversion, til understøttelse af Firefox ESR-tilbageførsler">
<correction safe-rm 					"Forhindrer installering i (og dermed ødelæggende) sammenlagte /usr-miljøer">
<correction sorl-thumbnail 				"Omgåelse af pgmagick-exception">
<correction sssd 					"sysdb: Renser søgefilters inddata [CVE-2017-12173]">
<correction tigervnc 					"Sikkerhedsopdateringer [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc 					"Sikkerhedsrettelser [CVE-2014-6053 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-8287 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction tmpreaper 					"Tilføjer <q>--protect '/tmp/systemd-private*/*'</q> til cronjob for at forhindre at systemd-services holder op med at fungere hvis de har PrivateTmp=true">
<correction tzdata 					"Ny opstrømsudgave">
<correction ublock-origin 				"Ny opstrømsversion, kompatibel med Firefox ESR68">
<correction unhide 					"Retter stakudmattelse">
<correction x2goclient 					"Fjerner ~/, ~user{,/}, ${HOME}{,/} og $HOME{,/} fra destinationsstier i scp-tilstand; retter regression med nyere versioner af libssh med anvendte rettelser af CVE-2019-14889">
<correction xml-security-c 				"Retter <q>DSA-verifikation får OpenSSL til at gå ned ved ugyldige kombinationer af nøgleindhold</q>">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den gamle stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2019 4474 firefox-esr>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4509 apache2>
<dsa 2019 4509 subversion>
<dsa 2019 4511 nghttp2>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4522 faad2>
<dsa 2019 4523 thunderbird>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4528 bird>
<dsa 2019 4529 php7.0>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux>
<dsa 2019 4532 spip>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4537 file-roller>
<dsa XXXX 4539 openssl>
<dsa 2019 4540 openssl1.0>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4548 openjdk-8>
<dsa XXXX 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4552 php7.0>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4557 libarchive>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4564 linux>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4571 thunderbird>
<dsa 2019 4573 symfony>
<dsa 2019 4574 redmine>
<dsa 2019 4576 php-imagick>
<dsa 2019 4578 libvpx>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4587 ruby2.3>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4594 openssl1.0>
<dsa 2019 4595 debian-lan-config>
<dsa 2019 4596 tomcat8>
<dsa XXXX 4596 tomcat-native>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4607 openconnect>
<dsa 2020 4609 python-apt>
<dsa XXXX 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4614 sudo>
<dsa 2020 4615 spamassassin>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>		<th>Årsag</th></tr>
<correction firetray 		"Inkompatibel med aktuelle versioner af Thunderbird">
<correction koji 		"Sikkerhedsproblemer">
<correction python-lamson 	"Defekt på grund af ændringer i python-daemon">
<correction radare2 		"Sikkerhedsproblemer; opstrøm tilbyder ikke support af stabil version">
<correction ruby-simple-form 	"Benyttes ikke; sikkerhedsproblemer">
<correction trafficserver 	"Kan ikke understøttes">
</table>


<h2>Debian Installer</h2>

Installeringsprogrammet er opdateret for at medtage rettelser indført i oldstable, 
i denne punktopdatering.


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle gamle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Foreslåede opdateringer til den gamle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Oplysninger om den gamle stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>
