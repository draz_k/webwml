#use wml::debian::translation-check translation="9bf43d9ebe1fe8f0f648cd5c0431b530d1105d92" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er fundet i Apaches HTTP-server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17189">CVE-2018-17189</a>

    <p>Gal Goldshtein fra F5 Networks opdagede en lammelsesangrebssårbarhed i 
    mod_http2.  Ved at sende misdannede forespørgsler, lagde http/2-strømmen til 
    denne forespørgsel unødvendigt beslag på en servertråd til oprydning af 
    indkommende data, medførende lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17199">CVE-2018-17199</a>

    <p>Diego Angulo fra ImExHS opdagede at mod_session_cookie ikke respekterede 
    udløbstiden.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0196">CVE-2019-0196</a>

    <p>Craig Young opdagede at http/2-forespørgselshåndteringen i mod_http2 
    kunne bringes til at tilgå frigivet hukommelse i strengsammenligninger, 
    når forespørgslens metode blev afgjort, og dermed blev forespørgsel 
    behandlet forkert.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0211">CVE-2019-0211</a>

    <p>Charles Fol opdagede en rettighedsforøgelse fra en mindre priviligeret 
    barneproces til forælderprocessen, der kører som root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0217">CVE-2019-0217</a>

    <p>En kapløbstilstand i mod_auth_digest, når der køres i en 
    threaded server, kunne gøre det muligt for en bruger med gyldige 
    loginoplysninger, at autentificere sig med et andet brugernavn, og dermed 
    omgå opsatte adgangskontrolbegrænsninger.  Problemet blev opdaget af Simon 
    Kappel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0220">CVE-2019-0220</a>

    <p>Bernhard Lorenz fra Alpha Strike Labs GmbH rapporterede at 
    URL-normaliseringerne blev behandlet inkonsekvent.  Når stikomponenten i 
    en forespørgsels-URL indeholder adskillige på hinanden følgende skråstreger 
    ('/'), tog direktiver så som LocationMatch og RewriteRule hensyn til 
    duplikater i reuglære udtræk, mens andre aspekter ved serverbehandlingen 
    implicit slog dem sammen.</p></li>

</ul>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 2.4.25-3+deb9u7.</p>

<p>Denne opdatering indeholder også fejlrettelser, der var planlagt til at blive 
medtaget i den næste stabile punktopdatering.  Herunder en rettelse af en 
regression, forårsaget af en sikkerhedsrettelse i version 2.4.25-3+deb9u6.</p>

<p>Vi anbefaler at du opgraderer dine apache2-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende apache2, se
dens sikkerhedssporingssidede på: 
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4422.data"
