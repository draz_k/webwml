#use wml::debian::translation-check translation="2f2e4e4ed7b781eff447b99c3d177c672b61e21f" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Tim Starling opdagede to sårbarheder i firejail, et sandkasseprogram til 
begrænsning af afviklingsmiljøet for programmer, der ikke er tillid til.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17367">CVE-2020-17367</a>

    <p>Der blev rapporteret at firejail ikke respekterede 
    end-of-options-separatoren (<q>--</q>), hvilke gjorde det muligt for en 
    angriber med kontrol over kommandolinjeparametrene hørende til et program i 
    sandkassen, at skrive data til en angivet fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17368">CVE-2020-17368</a>

    <p>Der blev rapporteret at firejail, når der viderestilles uddata gennem 
    --output eller --output-stderr, så samles alle kommandolinjeparametre i 
    en enkelt streng, som overføres til en shell.  En angriber, der har kontrol 
    over kommandolinjeparametrene hørende til et program i sandkassen, kunne 
    drage nytte af fejlen til at udføre vilkårlige kommandoer.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 0.9.58.2-2+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine firejail-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende firejail, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/firejail">\
https://security-tracker.debian.org/tracker/firejail</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4742.data"
